FROM node:16
WORKDIR /app
COPY package.json /app/
COPY yarn.lock /app/
RUN SKIP_POSTINSTALL=1 yarn install
COPY . /app
RUN yarn postinstall
CMD ["yarn", "start"]