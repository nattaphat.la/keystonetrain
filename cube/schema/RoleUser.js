cube(`RoleUser`, {
  sql: `SELECT * FROM public."_Role_user"`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: []
    }
  },
  
  dimensions: {
    b: {
      sql: `${CUBE}."B"`,
      type: `string`
    },
    
    a: {
      sql: `${CUBE}."A"`,
      type: `string`
    }
  },
  
  dataSource: `default`
});
