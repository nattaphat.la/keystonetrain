cube(`Post`, {
  sql: `SELECT * FROM public."Post"`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    User: {
      relationship: `belongsTo`,
      sql: `${User}.id = ${Post}.author`,
    },
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [title, id, publishdate]
    }
  },
  
  dimensions: {
    author: {
      sql: `author`,
      type: `string`
    },
    
    title: {
      sql: `title`,
      type: `string`
    },
    
    status: {
      sql: `status`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    content: {
      sql: `content`,
      type: `string`
    },
    
    publishdate: {
      sql: `${CUBE}."publishDate"`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
