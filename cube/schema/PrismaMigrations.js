cube(`PrismaMigrations`, {
  sql: `SELECT * FROM public._prisma_migrations`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id, migrationName]
    },
    
    appliedStepsCount: {
      sql: `applied_steps_count`,
      type: `sum`
    }
  },
  
  dimensions: {
    logs: {
      sql: `logs`,
      type: `string`
    },
    
    checksum: {
      sql: `checksum`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    migrationName: {
      sql: `migration_name`,
      type: `string`
    },
    
    startedAt: {
      sql: `started_at`,
      type: `time`
    },
    
    rolledBackAt: {
      sql: `rolled_back_at`,
      type: `time`
    },
    
    finishedAt: {
      sql: `finished_at`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
