cube(`User`, {
  sql: `SELECT * FROM public."User"`,
  
  preAggregations: {
    // Pre-Aggregations definitions go here
    // Learn more here: https://cube.dev/docs/caching/pre-aggregations/getting-started  
  },
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [name, id]
    }
  },
  
  dimensions: {
    isadmin: {
      sql: `${CUBE}."isAdmin"`,
      type: `string`
    },
    
    name: {
      sql: `name`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `string`,
      primaryKey: true
    },
    
    password: {
      sql: `password`,
      type: `string`
    },
    
    email: {
      sql: `email`,
      type: `string`
    }
  },
  
  dataSource: `default`
});
