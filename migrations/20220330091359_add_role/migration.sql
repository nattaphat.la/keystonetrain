/*
  Warnings:

  - You are about to drop the `Task` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_Task_assignTo` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_Task_assignTo" DROP CONSTRAINT "_Task_assignTo_A_fkey";

-- DropForeignKey
ALTER TABLE "_Task_assignTo" DROP CONSTRAINT "_Task_assignTo_B_fkey";

-- DropTable
DROP TABLE "Task";

-- DropTable
DROP TABLE "_Task_assignTo";

-- CreateTable
CREATE TABLE "Role" (
    "id" UUID NOT NULL,
    "name" TEXT NOT NULL DEFAULT E'',

    CONSTRAINT "Role_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_Role_user" (
    "A" UUID NOT NULL,
    "B" UUID NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_Role_user_AB_unique" ON "_Role_user"("A", "B");

-- CreateIndex
CREATE INDEX "_Role_user_B_index" ON "_Role_user"("B");

-- AddForeignKey
ALTER TABLE "_Role_user" ADD FOREIGN KEY ("A") REFERENCES "Role"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Role_user" ADD FOREIGN KEY ("B") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
